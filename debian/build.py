#!/usr/bin/fontforge
# simple build script

import fontforge, sys;
required_version = "20070501"

if fontforge.version() < required_version:
  print ("Your version of FontForge is too old - %s or newer is required" % (required_version));

i = 1
while i < len(sys.argv):
  font=fontforge.open(sys.argv[i]);

  fontname = font.fontname + ".ttf";
  print (" ");
  print ("Building ") + font.fullname + (" from font source: ") + font.fontname + (".sfd...");
  print ("Done.");
  # layer="TTF" from ost-generate.py
  font.generate(fontname,"",layer="TTF");
  font.close();
  i += 1;

print (" ");
